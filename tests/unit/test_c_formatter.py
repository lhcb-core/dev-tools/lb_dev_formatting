from unittest.mock import patch
from pyOptional import Optional

from lb_dev_formatting.formatter.c_formatter import CFormatter
from tests.temporary_directory_handler import TemporaryDirecotryHandler


@patch('lb_dev_formatting.formatter.c_formatter.CFormatter.find_command')
def test_check_if_format_command_is_available(mock_find_command):
    mock_find_command.return_value = 'clang-format-8'

    formatter = CFormatter('8')

    assert formatter.is_clang_format_available


@patch('lb_dev_formatting.formatter.c_formatter.CFormatter.find_command')
def test_format_clang_not_available(mock_find_command):
    mock_find_command.return_value = None

    formatter = CFormatter('12')

    assert Optional.empty() == formatter.format_file('some_file.c')


@patch('lb_dev_formatting.formatter.c_formatter.CFormatter.find_command')
def test_format_empty_file(mock_find_command):
    mock_find_command.return_value = 'clang-format-8'

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = './{}/some_file.c'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.close()

        formatter = CFormatter('8')

        assert Optional({
            'before': b'',
            'after': b''
        }) == formatter.format_file(file_name)


@patch('lb_dev_formatting.formatter.c_formatter.CFormatter.apply_formating')
@patch('lb_dev_formatting.formatter.c_formatter.CFormatter.find_command')
def test_format(mock_find_command, mock_apply_formatting):
    mock_find_command.return_value = 'clang-format-8'
    mock_apply_formatting.return_value = Optional('some text formatted')

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = './{}/some_file.c'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.write('some text')
        temp_file.close()

        formatter = CFormatter('8')

        assert Optional({
            'before': b'some text',
            'after': 'some text formatted'
        }) == formatter.format_file(file_name)


@patch(
    'lb_dev_formatting.formatter.c_formatter.FileUtils.ensure_file_exists_in_repository'
)
@patch('lb_dev_formatting.formatter.c_formatter.ShellUtils.run_in_shell')
def test_apply_formating(mock_run_in_shell,
                         mock_ensure_file_exists_in_repository):
    mock_run_in_shell.return_value = 'some text formatted'
    mock_ensure_file_exists_in_repository.return_value = '4'

    formatter = CFormatter('8')

    result = formatter.apply_formating('some text', 'some_file.c')

    assert Optional('some text formatted') == result
