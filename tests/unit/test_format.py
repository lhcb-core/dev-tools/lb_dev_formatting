from lb_dev_formatting.commands.format import check_for_illegal_arguments, format_command, format_files, add_headers_to_message
from lb_dev_formatting.exceptions.illegal_argument import IllegalArgumentError

from tests.temporary_directory_handler import TemporaryDirecotryHandler

from pytest import raises
from unittest.mock import patch
from pyOptional import Optional
from email.message import Message
from email.utils import formatdate


def test_check_illegal_arguments():
    assert check_for_illegal_arguments('some_file.py', None, False, None) == 0


def test_check_illegal_arguments_pipe_and_patch_raise_exception():
    with raises(IllegalArgumentError):
        assert check_for_illegal_arguments([], None, True, 'c')


def test_check_illegal_arguments_pipe_and_reference_raise_exception():
    with raises(IllegalArgumentError):
        assert check_for_illegal_arguments([], 'some reference', False, 'c')


def test_check_illegal_arguments_pipe_and_files_raise_exception():
    with raises(IllegalArgumentError):
        assert check_for_illegal_arguments(['some file'], None, False, 'c')


def test_check_illegal_arguments_reference_and_files_raise_exception():
    with raises(IllegalArgumentError):
        assert check_for_illegal_arguments(['some file'], 'some reference',
                                           False, None)


@patch(
    'lb_dev_formatting.formatter.formatter_facade.FormatterFacade.format_in_pipeline'
)
def test_format_files_in_pipeline(mock_format_in_pipeline):
    result = format_files(files=[],
                          clang_format_version='8',
                          yapf_version='8',
                          dry_run=False,
                          reference=None,
                          format_patch=None,
                          pipe='py')

    assert result == 0
    assert mock_format_in_pipeline.called


@patch('lb_utils.file_utils.FileUtils.get_files')
@patch(
    'lb_dev_formatting.formatter.formatter_facade.FormatterFacade.format_file')
def test_format_files_get_files_from_reference(mock_format_file,
                                               mock_get_files):
    mock_format_file.return_value = Optional('some formatted text')
    mock_get_files.return_value = ['some_file.py', 'some_other_file.c']
    format_files(files=[],
                 clang_format_version='8',
                 yapf_version='8',
                 dry_run=False,
                 reference='somre reference',
                 format_patch=None,
                 pipe=None)

    assert mock_format_file.called
    assert mock_get_files.called


@patch(
    'lb_dev_formatting.formatter.formatter_facade.FormatterFacade.format_file')
def test_format_files(mock_format_file):
    mock_format_file.return_value = Optional('some formatted text')
    format_files(files=['some_file.py'],
                 clang_format_version='8',
                 yapf_version='8',
                 dry_run=False,
                 reference=None,
                 format_patch=None,
                 pipe=None)

    assert mock_format_file.called


@patch('lb_dev_formatting.commands.format.create_formatting_patch')
@patch(
    'lb_dev_formatting.formatter.formatter_facade.FormatterFacade.format_file')
def test_format_files_creates_patch(mock_format_file,
                                    mock_create_formatting_patch):
    mock_format_file.return_value = Optional({
        'before': b'some text',
        'after': b'some formatted text'
    })

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file = get_temp_file('{}/some_file.py'.format(folder_name),
                             'some text')
        format_files(files=[file],
                     clang_format_version='8',
                     yapf_version='8',
                     dry_run=False,
                     reference=None,
                     format_patch='some_file.txt',
                     pipe=None)

        assert mock_create_formatting_patch.called


def test_add_headers_to_message():
    message = Message()
    message = add_headers_to_message(message)

    assert message._headers.sort() == [('From', 'Gitlab CI <noreply@cern.ch>'),
                                       ('Date', formatdate()),
                                       ('Subject', '[PATCH] Fixed formatting')
                                       ].sort()


def get_temp_file(path, content=None):
    temp_file1 = open(path, "w+")
    if content:
        temp_file1.writelines(content)
    temp_file1.close()

    return path
