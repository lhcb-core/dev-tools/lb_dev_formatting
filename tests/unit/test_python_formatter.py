from lb_dev_formatting.formatter.python_formatter import PythonFormatter

from tests.temporary_directory_handler import TemporaryDirecotryHandler

from unittest.mock import patch
from pyOptional import Optional


@patch('lb_dev_formatting.formatter.python_formatter.ShellUtils.find_command')
@patch('lb_dev_formatting.formatter.python_formatter.check_output')
def test_if_format_command_is_available(mock_check_output, mock_find_command):
    mock_check_output.return_value = b'yapf version 26'

    python_formatter = PythonFormatter('26')

    assert python_formatter.is_yapf_available


@patch('lb_dev_formatting.formatter.python_formatter.ShellUtils.find_command')
@patch('lb_dev_formatting.formatter.python_formatter.check_output')
def test_format_empty_file(mock_check_output, mock_find_command):
    mock_check_output.return_value = b'yapf version 26'

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = './{}/some_file.py'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.close()

        python_formatter = PythonFormatter('26')

        result = python_formatter.format_file(file_name)

        assert Optional({'before': b'', 'after': b''}) == result


@patch('lb_dev_formatting.formatter.python_formatter.ShellUtils.find_command')
@patch('lb_dev_formatting.formatter.python_formatter.check_output')
def test_format_file_yapf_not_available(mock_check_output, mock_find_command):
    mock_check_output.return_value = b'yapf version 26'

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = './{}/some_file.py'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.close()

        python_formatter = PythonFormatter('not 26')

        result = python_formatter.format_file(file_name)

        assert Optional.empty() == result


@patch('lb_dev_formatting.formatter.python_formatter.ShellUtils.run_in_shell')
@patch('lb_dev_formatting.formatter.python_formatter.ShellUtils.find_command')
@patch('lb_dev_formatting.formatter.python_formatter.check_output')
def test_format_file(mock_check_output, mock_find_command, mock_run_in_shell):
    mock_check_output.return_value = b'yapf version 26'
    mock_run_in_shell.return_value = 'some text formatted'

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = './{}/some_file.py'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.write('some text')
        temp_file.close()

        python_formatter = PythonFormatter('26')

        result = python_formatter.format_file(file_name)

        assert Optional({
            'before': b'some text',
            'after': 'some text formatted'
        }) == result
