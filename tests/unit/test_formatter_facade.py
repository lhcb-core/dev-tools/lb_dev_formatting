from lb_dev_formatting.formatter.formatter_facade import FormatterFacade
from lb_dev_formatting.formatter.python_formatter import PythonFormatter
from lb_dev_formatting.formatter.c_formatter import CFormatter
from lb_dev_formatting import constants

from tests.temporary_directory_handler import TemporaryDirecotryHandler

from unittest.mock import patch
from pytest import raises
from pyOptional import Optional


def test_formatter_facade_init():
    formatter_facade = FormatterFacade('some_yapf_version',
                                       'some_clang_version', False, False)

    assert type(formatter_facade.python_formatter) is PythonFormatter
    assert formatter_facade.python_formatter.yapf_version == 'some_yapf_version'
    assert type(formatter_facade.c_formatter) is CFormatter
    assert formatter_facade.c_formatter.clang_version == 'some_clang_version'
    assert formatter_facade.shouldReturnDiff is False
    assert formatter_facade.shouldOverwrite is False


def test_formatter_facade_init_with_defaults():
    formatter_facade = FormatterFacade()

    assert type(formatter_facade.python_formatter) is PythonFormatter
    assert formatter_facade.python_formatter.yapf_version == constants.YAPF_VERSION
    assert type(formatter_facade.c_formatter) is CFormatter
    assert formatter_facade.c_formatter.clang_version == constants.CLANG_FORMAT_VERSION
    assert formatter_facade.shouldReturnDiff is False
    assert formatter_facade.shouldOverwrite is True


def test_get_appropriate_fomratter_python():
    formatter_facade = FormatterFacade()

    formatter = formatter_facade.get_appropriate_formatter('some_file.py')

    assert type(formatter) is PythonFormatter


def test_get_appropriate_fomratter_c():
    formatter_facade = FormatterFacade()

    formatter = formatter_facade.get_appropriate_formatter('some_file.c')

    assert type(formatter) is CFormatter


def test_get_appropriate_fomratter_random_extension():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = './{}/some_file.random_extension'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.write('this is some text')
        temp_file.close()

        formatter_facade = FormatterFacade()

        formatter = formatter_facade.get_appropriate_formatter(file_name)

        assert formatter is None


def get_appropriate_formatter_by_file_extension_py():
    formatter_facade = FormatterFacade()

    formatter = formatter_facade.get_appropriate_formatter_by_file_extension(
        'py')

    assert type(formatter) is PythonFormatter


def get_appropriate_formatter_by_file_extension_c():
    formatter_facade = FormatterFacade()

    formatter = formatter_facade.get_appropriate_formatter_by_file_extension(
        'c')

    assert type(formatter) is CFormatter


def get_appropriate_formatter_by_file_extension_random_extension():
    formatter_facade = FormatterFacade()

    formatter = formatter_facade.get_appropriate_formatter_by_file_extension(
        'random_extension')

    assert formatter is None


@patch('lb_dev_formatting.formatter.formatter_facade.PythonFormatter')
def test_format_in_pipeline_py(python_formatter_mock):
    formatter = FormatterFacade()

    formatter.format_in_pipeline('py')

    assert python_formatter_mock.called


@patch('lb_dev_formatting.formatter.formatter_facade.CFormatter')
def test_format_in_pipeline_c(c_formatter_mock):
    formatter = FormatterFacade()

    formatter.format_in_pipeline('c')

    assert c_formatter_mock.called


def test_format_in_pipeline_random_extension():
    formatter = FormatterFacade()

    with raises(AttributeError):
        formatter.format_in_pipeline('random_extension')


@patch(
    'lb_dev_formatting.formatter.formatter_facade.FormatterFacade.handle_formatting_result'
)
@patch(
    'lb_dev_formatting.formatter.formatter_facade.PythonFormatter.format_file')
def test_format(mock_python_formatter, mock_handle_formatting_result):
    formatter_facade = FormatterFacade()

    mock_handle_formatting_result.return_value = 'some value'
    result = formatter_facade.format_file('some_file.py')

    assert mock_python_formatter.called
    assert result == 'some value'


def test_handle_formatting_result():
    formatter_facade = FormatterFacade()

    assert Optional.empty() == formatter_facade.handle_formatting_result(
        Optional.empty(), 'some_file.py')


def test_handle_formatting_result_return_diff():
    formatter_facade = FormatterFacade(shouldReturnDiff=True,
                                       shouldOverwrite=False)

    formatting_result = Optional({'before': 'result1', 'after': 'result2'})

    result = formatter_facade.handle_formatting_result(formatting_result,
                                                       'some_file.py')

    assert formatting_result == result


@patch('lb_dev_formatting.formatter.formatter_facade.FileUtils.write_to_file')
def test_test_handle_formatting_result_not_overwrite(mock_write_to_file):
    formatter_facade = FormatterFacade(shouldReturnDiff=False,
                                       shouldOverwrite=False)

    formatting_result = Optional({'before': 'result1', 'after': 'result2'})

    result = formatter_facade.handle_formatting_result(formatting_result,
                                                       'some_file.py')

    assert formatting_result == result
    assert not mock_write_to_file.called


@patch('lb_dev_formatting.formatter.formatter_facade.FileUtils.write_to_file')
def test_test_handle_formatting_result_overwrite(mock_write_to_file):
    formatter_facade = FormatterFacade(shouldReturnDiff=False,
                                       shouldOverwrite=True)

    formatting_result = Optional({'before': 'result1', 'after': 'result2'})

    result = formatter_facade.handle_formatting_result(formatting_result,
                                                       'some_file.py')

    assert formatting_result == result
    assert mock_write_to_file.called


@patch('lb_dev_formatting.formatter.formatter_facade.FileUtils.write_to_file')
def test_test_handle_formatting_result_before_equal_after(mock_write_to_file):
    formatter_facade = FormatterFacade(shouldReturnDiff=False,
                                       shouldOverwrite=False)

    formatting_result = Optional({'before': 'result1', 'after': 'result1'})

    result = formatter_facade.handle_formatting_result(formatting_result,
                                                       'some_file.py')

    assert formatting_result == result
    assert not mock_write_to_file.called
